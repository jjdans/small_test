<!DOCTYPE html> 
<html> 
	<head> 
		<title>Small Test - Error de conexión</title> 
	</head> 
	<body> 
		<h1>El sistema no puede establecer conexión con la Base de Datos.</h1> 
		<p>Esto puede deberse a que el servidor de Base de Datos se encuentra apagado o fue movido de ubicación.</p>
		<p>Intente establecer conexión dentro de unos minutos o avise al administrador.</p>
		<div>
			<a href="index.php">Volver a probar</a>
		</div>
	</body> 
</html>

