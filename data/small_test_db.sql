-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-05-2017 a las 04:56:01
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `small_test`
--
CREATE DATABASE IF NOT EXISTS `small_test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `small_test`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` varchar(50) NOT NULL,
  `title` varchar(45) NOT NULL,
  `date` date DEFAULT NULL,
  `body` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `post`
--

INSERT INTO `post` (`id`, `title`, `date`, `body`) VALUES
('1', 'Hablando de Pelota', '2017-02-15', 'Grupos de aficionados de la polata debaten sobre los resultados de Cuba'),
('2', 'Conociendo el Beisbol', '2017-04-29', ''),
('7d6f8b90fe6b75ce5a1de73729e782325fac4877', 'Quien gana la 56 serie de baseball', '2017-04-28', ''),
('7d9f7462e509527cbea71a0b44dcf5e6f5f832d9', 'Test2', '2017-05-11', 'TEST 2'),
('a99395545bdcf90cab58e84bff7fe3d598e05a93', 'Test3 mamÃ¡', '2017-05-19', 'Test3 para mamÃ¡'),
('af5fb8bdb3bc2e0a9ae363fdb8fc67d6764cd69a', 'Test 4', '2017-05-29', 'Test 4 para todas las pruebas que debemos realizar.'),
('cee1940239e9cfcbca58f5fcd4b0f4eb5edd5208', 'Test1', '2017-05-29', 'TEST 1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(35) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `role` enum('ADMIN','SUPERVISOR','USER') NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`username`, `first_name`, `last_name`, `email`, `role`, `password`) VALUES
('lolo', 'Lolo', 'Moreno Silverio', 'lolo@smalltest.com', 'USER', 'lolo'),
('pepe', 'Pepe', 'Gonzalez Lopez', 'pepe@smalltest.com', 'ADMIN', 'pepe'),
('test1', 'Test1', 'Prueba1', 'test1@sdfsdfsdf.cu', 'SUPERVISOR', 'test1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
