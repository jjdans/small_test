<?php

/**
 * Description of AbstractController
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
abstract class AbstractController {

	public static function getErrorPage($code) {
		$page = 'web\common\error500.php';
		if ($code == '403') {
			$page = 'web\common\error403.php';
		}
		return $page;
	}

}
