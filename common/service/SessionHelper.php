<?php

require_once 'common\service\Session.php';

/**
 * Description of SessionHelper
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class SessionHelper {

	static $SESSION_ATTRIB_FLASH = "flash";

	/**
	 * 
	 * @param type $msg
	 * @return type
	 */
	public static function getFlash($msg = null) {
		$session = new Session();
		if (!is_null($msg)) {
			$session->setAttribute(SessionHelper::$SESSION_ATTRIB_FLASH, $msg);
			return;
		} else {
			$msg = $session->getAttribute(SessionHelper::$SESSION_ATTRIB_FLASH);
			if (!is_null($msg)) {
				$session->removeAttribute(SessionHelper::$SESSION_ATTRIB_FLASH);
			}
			return $msg;
		}
	}

}
