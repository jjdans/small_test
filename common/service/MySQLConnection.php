<?php

/**
 * Description of MySQLConnection
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class MySQLConnection {

	private $server = 'localhost';
	private $user = 'root';
	private $password = '';
	private $database = 'small_test';

	/**
	 * Get server
	 * 
	 * @return string
	 */
	public function getServer() {
		return $this->server;
	}

	/**
	 * Get user
	 * 
	 * @return string
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Get password
	 * 
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * Get database
	 * 
	 * @return string
	 */
	public function getDatabase() {
		return $this->database;
	}

	/**
	 * Set server
	 * @param string $server
	 */
	public function setServer($server) {
		$this->server = $server;
	}

	/**
	 * Set user
	 * @param string $user
	 */
	public function setUser($user) {
		$this->user = $user;
	}

	/**
	 * Set password
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * Set database
	 * 
	 * @param string $database
	 */
	public function setDatabase($database) {
		$this->database = $database;
	}

	/**
	 * Open the connection
	 * 
	 * @return connection
	 */
	public function openDatabaseConnection() {
		$link = mysqli_connect($this->getServer(), $this->getUser(), $this->getPassword(), $this->getDatabase());
		if (!$link) {
			$this->redirectToReconfigure();
		}else{
			return $link;
		}
	}

	function redirectToReconfigure() {
		$host = $_SERVER['HTTP_HOST'];
		$uri = str_replace('index.php', 'connection.php', $_SERVER['SCRIPT_NAME']);
		if (!isset($_SERVER['HTTPS'])) {
			header("Location: http://{$host}{$uri}");
		} else {
			header("Location: https://{$host}{$uri}");
		}
		exit();
	}

	/**
	 * Close the connection
	 * @param Connection $link
	 */
	public function closeDatabaseConnection($link) {
		if ($link) {
			mysqli_close($link);
		}
	}

}
