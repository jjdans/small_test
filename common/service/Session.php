<?php

/**
 * Description of Session
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class Session {

	public function Session() {
		@session_start();
	}

	public function getAttribute($attribute) {
		if (!isset($_SESSION[$attribute])) {
			return null;
		}
		return $_SESSION[$attribute];
	}

	public function setAttribute($attribute, $value) {
		$_SESSION[$attribute] = $value;
		return;
	}

	public function removeAttribute($attribute) {
		if (isset($_SESSION[$attribute])) {
			unset($_SESSION[$attribute]);
		}
		return;
	}
	
	public function destroy(){
		session_destroy();
		return;
	} 

}
