<?php

// route the request internally 
$uri = $_SERVER['REQUEST_URI'];

// proyect name
$proyectName = '/SmallTest';

if ($proyectName . '/index.php/login' == $uri) {
	$securityController = new SecurityController();
	$securityController->loginAction();
} elseif ($proyectName . '/index.php/logout' == $uri) {
	$securityController = new SecurityController();
	$securityController->logoutAction();
} else {
	require 'web/common/error404.php';
}