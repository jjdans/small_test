<?php

/**
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
interface EntityItem {

	public function getId();
}
