<?php

require_once 'security/dao/UserDaoImpl.php';

/**
 * Description of AbstractService
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.com y jjdans87@gmail.com
 */
abstract class AbstractManager {

	protected $USER_INVALID = "Not a valid user";
	protected $USER_NOT_ADMIN = "Not an admin user";
	protected $INTERNAL_ERROR = "Internal error";

	/**
	 * Check if the user is valid
	 * 
	 * @param string $username
	 * @return boolean
	 */
	protected function isValidUser($username) {
		$userDao = new UserDaoImpl();
		$user = $userDao->find($username);
		return $user->getUsername() != null;
	}

}
