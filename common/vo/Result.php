<?php

/**
 * Description of Result
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class Result {

	private $success;
	private $data;
	private $msg;

	/**
	 * Contructor
	 * @param type $success
	 * @param type $data
	 * @param type $msg
	 */
	public function Result($success, $data, $msg) {
		$this->success = $success;
		$this->data = $data;
		$this->msg = $msg;
	}

	/**
	 * Get success
	 * 
	 * @return boolean
	 */
	public function isSuccess() {
		return $this->success;
	}

	/**
	 * Get data
	 * 
	 * @return Object
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * Get msg
	 * 
	 * @return string
	 */
	public function getMsg() {
		return $this->msg;
	}
}
