<?php

/**
 * Description of ResultFactory
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class ResultFactory {

	public static function getSuccessResult($data) {
		return new Result(true, $data, "");
	}

	public static function getSuccessResultDataMsg($data, $msg) {
		return new Result(true, $data, $msg);
	}

	public static function getSuccessResultMsg($msg) {
		return new Result(true, null, $msg);
	}

	public static function getFailResult($data, $msg) {
		return new Result(false, $data, $msg);
	}

}