<?php

/**
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
interface PostDao {

	public function persist($object);

	public function merge($object);

	public function remove($id);

	public function find($id);

	public function findAll();
}
