<?php

require_once 'moduleA\dao\PostDao.php';
require_once 'common\service\MySQLConnection.php';
require_once 'moduleA\domain\Post.php';

/**
 * Description of PostDaoImpl
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class PostDaoImpl implements PostDao {

	public function find($id) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$query = "SELECT id, title, date, body FROM post WHERE id = '" . $id . "'";
		$result = mysqli_query($conn, $query);
		if (!$result) {
			return false;
		} else {
			$total = mysqli_num_rows($result);
			$row = mysqli_fetch_assoc($result);
			$post = new Post();
			$post->setId($row['id']);
			$post->setTitle($row['title']);
			$post->setDate($row['date']);
			$post->setBody($row['body']);
			mysqli_free_result($result);
			$mySQLConn->closeDatabaseConnection($conn);
			return $post;
		}
	}

	public function findAll() {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$result = mysqli_query($conn, 'SELECT id, title, date, body FROM post');
		if (!$result) {
			return false;
		} else {
			$posts = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$post = new Post();
				$post->setId($row['id']);
				$post->setTitle($row['title']);
				$post->setDate($row['date']);
				$post->setBody($row['body']);
				$posts[] = $post;
			}
			mysqli_free_result($result);
			$mySQLConn->closeDatabaseConnection($conn);
			return $posts;
		}
	}

	public function merge($object) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$date = 'NULL';
		if (!is_null($object->getDate())) {
			$date = "'" . $object->getDate() . "'";
		}
		$query = "UPDATE post SET title='" . $object->getTitle() . "', date=$date, body='" . $object->getBody() . "' WHERE id='" . $object->getId() . "'";
		$result = mysqli_query($conn, $query);
		if (!$result) {
			return false;
		}
		$mySQLConn->closeDatabaseConnection($conn);
		return $result;
	}

	public function persist($object) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$date = 'NULL';
		if (!is_null($object->getDate())) {
			$date = "'" . $object->getDate() . "'";
		}
		$title = $object->getTitle();
		$query = "INSERT INTO post (id, title, date, body) VALUES ('" . $object->getId() . "', '$title', $date, '" . $object->getBody() . "')";
		$result = mysqli_query($conn, $query);
		if (!$result) {
			return false;
		}
		$mySQLConn->closeDatabaseConnection($conn);
		return $result;
	}

	public function remove($id) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$result = mysqli_query($conn, "DELETE FROM post WHERE id='" . $id . "'");
		if (!$result) {
			return false;
		}
		$mySQLConn->closeDatabaseConnection($conn);
		return $result;
	}

}
