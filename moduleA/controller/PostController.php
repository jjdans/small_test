<?php

require_once 'common\controller\AbstractController.php';
require_once 'moduleA\manager\PostManagerImpl.php';
require_once 'security\service\SecurityHelper.php';
require_once 'common\vo\Result.php';
require_once 'common\service\SessionHelper.php';

/**
 * Description of PostController
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class PostController extends AbstractController {

	private $postManager;

	public function PostController() {
		$this->postManager = new PostManagerImpl();
	}

	public function findAction() {
		$result = $this->postManager->find($_GET['id'], SecurityHelper::getSessionUser());
		if ($result->isSuccess()) {
			$post = $result->getData();
			require 'web\moduleA\postShow.php';
		} else {
			$code = $result->getData();
			$page = $this->getErrorPage($code);
			require $page;
		}
	}

	public function findAllAction() {
		$result = $this->postManager->findAll(SecurityHelper::getSessionUser());
		if ($result->isSuccess()) {
			$posts = $result->getData();
			require 'web\moduleA\postList.php';
		} else {
			$code = $result->getData();
			$page = $this->getErrorPage($code);
			require $page;
		}
	}

	public function newAction() {
		if (SecurityHelper::getSessionUser() != null) {
			$msg = '';
			if ($_SERVER['REQUEST_METHOD'] == 'GET') {
				require 'web\moduleA\postNew.php';
			} else {
				$date = $_POST['date'] == '' ? null : $_POST['date'];
				if (is_null($_POST['title']) || $_POST['title'] == '') {
					$msg = 'El t&iacute;tulo es requerido';
					require 'web\moduleA\postNew.php';
				} else {
					$result = $this->postManager->store(null, $_POST['title'], $date, $_POST['body'], SecurityHelper::getSessionUser());
					if ($result->isSuccess()) {
						$data = $result->getMsg();
						SessionHelper::getFlash($data);
						$host = $_SERVER['HTTP_HOST'];
						$uri = '/SmallTest/index.php/post/new';
						header("Location: http://{$host}{$uri}");
					} else {
						$code = $result->getData();
						$page = $this->getErrorPage($code);
						require $page;
					}
				}
			}
		} else {
			require 'web\common\error403.php';
		}
	}

	public function editAction() {
		if (SecurityHelper::getSessionUser() != null) {
			$msg = '';
			if ($_SERVER['REQUEST_METHOD'] == 'GET') {
				$result = $this->postManager->find($_GET['id'], SecurityHelper::getSessionUser());
				if ($result->isSuccess()) {
					$post = $result->getData();
					require 'web\moduleA\postEdit.php';
				} else {
					$code = $result->getData();
					$page = $this->getErrorPage($code);
					require $page;
				}
			} else {
				$date = $_POST['date'] == '' ? null : $_POST['date'];
				$id = $_POST['id'];
				$result = $this->postManager->store($id, $_POST['title'], $date, $_POST['body'], SecurityHelper::getSessionUser());
				if ($result->isSuccess()) {
					$data = $result->getMsg();
					SessionHelper::getFlash($data);
					$host = $_SERVER['HTTP_HOST'];
					$uri = '/SmallTest/index.php/post/edit?id=' . $id;
					header("Location: http://{$host}{$uri}");
				} else {
					$code = $result->getData();
					if ($code === '501') {
						$data = $result->getMsg();
						SessionHelper::getFlash($data);
						$host = $_SERVER['HTTP_HOST'];
						$uri = '/SmallTest/index.php/post';
						header("Location: http://{$host}{$uri}");
					} else {
						$page = $this->getErrorPage($code);
						require $page;
					}
				}
			}
		} else {
			require 'web\common\error403.php';
		}
	}

	public function removeAction() {
		if (SecurityHelper::getSessionUser() != null) {
			if ($_SERVER['REQUEST_METHOD'] == 'GET') {
				$result = $this->postManager->find($_GET['id'], SecurityHelper::getSessionUser());
				if ($result->isSuccess()) {
					$post = $result->getData();
					require 'web\moduleA\postDelete.php';
				} else {
					$code = $result->getData();
					$page = $this->getErrorPage($code);
					require $page;
				}
			} else {
				$result = $this->postManager->remove($_POST['id'], SecurityHelper::getSessionUser());
				if ($result->isSuccess()) {
					$data = $result->getMsg();
					SessionHelper::getFlash($data);
					$host = $_SERVER['HTTP_HOST'];
					$uri = '/SmallTest/index.php/post';
					header("Location: http://{$host}{$uri}");
				} else {
					$code = $result->getData();
					if ($code === '501') {
						$data = $result->getMsg();
						SessionHelper::getFlash($data);
						$host = $_SERVER['HTTP_HOST'];
						$uri = '/SmallTest/index.php/post';
						header("Location: http://{$host}{$uri}");
					} else {
						$page = $this->getErrorPage($code);
						require $page;
					}
				}
			}
		} else {
			require 'web\common\error403.php';
		}
	}
}