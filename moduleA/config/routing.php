<?php

// route the request internally 
$uri = $_SERVER['REQUEST_URI'];

// proyect name
$proyectName = '/SmallTest';

if ($proyectName . '/index.php/post' == $uri) {
	$postController = new PostController();
	$postController->findAllAction();
} elseif ($proyectName . '/index.php/post/new' == $uri) {
	$postController = new PostController();
	$postController->newAction();
} elseif ($proyectName . '/index.php/post/edit' == $uri) {
	$postController = new PostController();
	$postController->editAction();
} elseif ($proyectName . '/index.php/post/delete' == $uri) {
	$postController = new PostController();
	$postController->removeAction();
} elseif (isset($_GET['id'])) {
	if ($proyectName . '/index.php/post/show?id=' . $_GET['id'] == $uri) {
		$postController = new PostController();
		$postController->findAction();
	} elseif ($proyectName . '/index.php/post/edit?id=' . $_GET['id'] == $uri) {
		$postController = new PostController();
		$postController->editAction();
	} elseif ($proyectName . '/index.php/post/delete?id=' . $_GET['id'] == $uri) {
		$postController = new PostController();
		$postController->removeAction();
	} else {
		require 'web/common/error404.php';
	}
} else {
	require 'web/common/error404.php';
}
