<?php

require_once 'common\domain\EntityItem.php';

/**
 * Description of Post
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class Post implements EntityItem {

	private $id;
	private $title;
	private $date;
	private $body;

	/**
	 * Contructor
	 */
	public function __construct() {
		$this->id = sha1(uniqid(rand(), true));
	}

	/*
	 * Get the identifier string
	 * 
	 * @return string
	 */

	public function __toString() {
		return $this->title;
	}

	/**
	 * Get table
	 * 
	 * @return string
	 */
	public function getTable() {
		return "post";
	}

	/**
	 * Get id
	 * 
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get title
	 * 
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Get date
	 * 
	 * @return string
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Get body
	 * 
	 * @return string
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * Set id
	 * 
	 * @param string $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * Set title
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Set date
	 * 
	 * @param string $date
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * Set body
	 * 
	 * @param string $body
	 */
	function setBody($body) {
		$this->body = $body;
	}

}
