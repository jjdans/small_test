<?php

require_once 'common\manager\AbstractManager.php';
require_once 'moduleA\manager\PostManager.php';
require_once 'common\vo\ResultFactory.php';
require_once 'moduleA\dao\PostDaoImpl.php';
require_once 'security\dao\UserDaoImpl.php';

/**
 * Description of PostManagerImpl
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class PostManagerImpl extends AbstractManager implements PostManager {

	protected $postDao;
	protected $userDao;

	public function PostManagerImpl() {
		$this->postDao = new PostDaoImpl();
		$this->userDao = new UserDaoImpl();
	}

	public function find($id, $actionUsername) {
		if ($this->isValidUser($actionUsername)) {
			$result = $this->postDao->find($id);
			if ($result) {
				return ResultFactory::getSuccessResult($result);
			} else {
				return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
			}
		} else {
			return ResultFactory::getFailResult('403', $this->USER_INVALID);
		}
	}

	public function findAll($actionUsername) {
		if ($this->isValidUser($actionUsername)) {
			$result = $this->postDao->findAll();
			if ($result) {
				return ResultFactory::getSuccessResult($result);
			} else {
				return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
			}
		} else {
			return ResultFactory::getFailResult('403', $this->USER_INVALID);
		}
	}

	public function remove($id, $actionUsername) {
		$actionUser = $this->userDao->find($actionUsername);
		if (!$actionUser->isAdmin()) {
			return ResultFactory::getFailResult('403', $this->USER_NOT_ADMIN);
		}
		if ($id == null) {
			return ResultFactory::getFailResult("501, Unable to remove Post [null id]");
		}
		$post = $this->postDao->find($id);
		if (is_null($post->getId())) {
			$msg = "The Post was removed by other user";
			return ResultFactory::getSuccessResultMsg($msg);
		} else {
			$result = $this->postDao->remove($id);
			if ($result) {
				$msg = "Post: " . $post->getTitle() . " was deleted by " . $actionUsername;
				return ResultFactory::getSuccessResultMsg($msg);
			} else {
				return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
			}
		}
	}

	public function store($id, $title, $date, $body, $actionUsername) {
		if ($this->isValidUser($actionUsername)) {
			$post;
			if (is_null($id)) {
				$post = new Post();
			} else {
				$post = $this->postDao->find($id);
				if (is_null($post->getId())) {
					return ResultFactory::getFailResult('501', "Unable to update Post instance because was removed.");
				}
			}
			$post->setTitle($title);
			$post->setDate($date);
			$post->setBody($body);
			$result = false;
			$msg = "";
			if (is_null($id)) {
				$result = $this->postDao->persist($post);
				$msg = "Post was registered correctly.";
			} else {
				$result = $this->postDao->merge($post);
				$msg = "Post was modified correctly.";
			}
			if ($result) {
				return ResultFactory::getSuccessResultDataMsg($post, $msg);
			} else {
				return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
			}
		} else {
			return ResultFactory::getFailResult('403', $this->USER_INVALID);
		}
	}

}
