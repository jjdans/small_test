<?php

/**
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
interface PostManager {

	public function store($id, $title, $date, $body, $actionUsername);

	public function remove($id, $actionUsername);

	public function find($id, $actionUsername);

	public function findAll($actionUsername);

}
