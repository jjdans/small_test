<?php

require_once 'common\manager\AbstractManager.php';
require_once 'security\manager\UserManager.php';
require_once 'common\vo\ResultFactory.php';
require_once 'security\dao\UserDaoImpl.php';

/**
 * Description of UserManagerImpl
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class UserManagerImpl extends AbstractManager implements UserManager {

	protected $userDao;

	public function USerManagerImpl() {
		$this->userDao = new UserDaoImpl();
	}

	public function exist($username) {
		$result = $this->userDao->find($username);
		if ($result) {
			return ResultFactory::getSuccessResult($result);
		} else {
			return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
		}
	}

	public function find($username, $actionUsername) {
		if ($this->isValidUser($actionUsername)) {
			$result = $this->userDao->find($username);
			if ($result) {
				return ResultFactory::getSuccessResult($result);
			} else {
				return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
			}
		} else {
			return ResultFactory::getFailResult('403', $this->USER_INVALID);
		}
	}

	public function findAll($actionUsername) {
		if ($this->isValidUser($actionUsername)) {
			$result = $this->userDao->findAll();
			if ($result) {
				return ResultFactory::getSuccessResult($result);
			} else {
				return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
			}
		} else {
			return ResultFactory::getFailResult('403', $this->USER_INVALID);
		}
	}

	public function findByUsernamePassword($username, $password) {
		if ((is_null($username) || $username == '') || (is_null($password) || $password == '')) {
			return ResultFactory::getFailResult('501', "The username or password are required.");
		} else {
			$user = $this->userDao->findByUsernamePassword($username, $password);
			if ($user) {
				if(!is_null($user->getUsername())){
					return ResultFactory::getSuccessResult($user);
				}else{
					return ResultFactory::getFailResult('501', "The username or password are incorrect.");
				}
			} else {
				return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
			}
		}
	}

	public function remove($username, $actionUsername) {
		$actionUser = $this->userDao->find($actionUsername);
		if (!$actionUser->isAdmin()) {
			return ResultFactory::getFailResult('403', $this->USER_NOT_ADMIN);
		}
		if (is_null($username)) {
			return ResultFactory::getFailResult('501', "Unable to remove User [null username]");
		} else if ($username == $actionUsername) {
			return ResultFactory::getFailResult('501', "Unable to remove User because not allowed to delete themselves");
		} else {
			$user = $this->userDao->find($username);
			if (is_null($user->getUsername())) {
				$msg = "The User was removed by other user";
				return ResultFactory::getSuccessResultMsg($msg);
			} else {
				$result = $this->userDao->remove($username);
				if ($result) {
					$msg = "User " . $user->getUsername() . " was deleted by " . $actionUsername;
					return ResultFactory::getSuccessResultMsg($msg);
				} else {
					return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
				}
			}
		}
	}

	public function store($username, $firstName, $lastName, $email, $password, $role, $actionUsername) {
		$actionUser = $this->userDao->find($actionUsername);
		if (!$actionUser->isAdmin()) {
			return ResultFactory::getFailResult('403', $this->USER_NOT_ADMIN);
		}
		$user;
		if (is_null($username) || empty($username) || is_null($firstName) || empty($firstName) || is_null($email) || empty($email)) {
			return ResultFactory::getFailResult("501", "Unable to store User [null username or first name or email]");
		} else if ($role != 'ADMIN' && $role != 'SUPERVISOR' && $role != 'USER') {
			return ResultFactory::getFailResult("501", "Unable to store User [Role is incorrect]");
		} else {
			$user = $this->userDao->find($username);
			if (is_null($user->getUsername())) {
				$user = new User();
			}
		}
		$user1 = $this->userDao->findByEmail($email);
		if (!is_null($user1->getUsername())) {
			if (!$user1->equals($user)) {
				return ResultFactory::getFailResult("501", "Unable to store because the mail exists [" . $email . "]");
			}
		}
		$user->setFirstName($firstName);
		$user->setLastName($lastName);
		$user->setEmail($email);
		$user->setPassword($password);
		$user->setRole($role);
		$result = false;
		if (!$user->getUsername()) {
			$user->setUsername($username);
			$result = $this->userDao->persist($user);
			$msg = "User " . $user->getUsername() . " was registered correctly.";
		} else {
			$result = $this->userDao->merge($user);
			$msg = "User " . $user->getUsername() . " was modified correctly.";
		}
		if ($result) {
			return ResultFactory::getSuccessResultMsg($msg);
		} else {
			return ResultFactory::getFailResult('500', $this->INTERNAL_ERROR);
		}
	}

}
