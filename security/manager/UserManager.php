<?php

/**
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
interface UserManager {

	public function store($username, $firstName, $lastName, $email, $password, $role, $actionUsername);

	public function remove($username, $actionUsername);

	public function find($username, $actionUsername);

	public function findAll($actionUsername);

	public function findByUsernamePassword($username, $password);
}
