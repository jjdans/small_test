<?php

require_once 'common\service\Session.php';

/**
 * Description of SecurityHelper
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class SecurityHelper {

	static $SESSION_ATTRIB_USERNAME = "username";
	static $SESSION_ATTRIB_FIRST_NAME = "first_name";
	static $SESSION_ATTRIB_LAST_NAME = "last_name";
	static $SESSION_ATTRIB_ROLE = "role";

	/**
	 * Get the username in the session
	 * 
	 * @return username or null
	 * 
	 */
	public static function getSessionUser() {
		$session = new Session();
		return $session->getAttribute(SecurityHelper::$SESSION_ATTRIB_USERNAME);
	}

	public static function getSessionFirstName() {
		$session = new Session();
		return $session->getAttribute(SecurityHelper::$SESSION_ATTRIB_FIRST_NAME);
	}

	public static function getSessionLastName() {
		$session = new Session();
		return $session->getAttribute(SecurityHelper::$SESSION_ATTRIB_LAST_NAME);
	}

	public static function getSessionFullName() {
		$session = new Session();
		return $session->getAttribute(SecurityHelper::$SESSION_ATTRIB_FIRST_NAME)." ".$session->getAttribute(SecurityHelper::$SESSION_ATTRIB_LAST_NAME);
	}
	
	public static function getSessionRole() {
		$session = new Session();
		return $session->getAttribute(SecurityHelper::$SESSION_ATTRIB_ROLE);
	}
}
