<?php

require_once 'security\dao\UserDao.php';
require_once 'common\service\MySQLConnection.php';
require_once 'security\domain\User.php';

/**
 * Description of UserDaoImpl
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class UserDaoImpl implements UserDao {

	public function find($username) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$query = "SELECT username, first_name, last_name, email, role FROM user WHERE username = '" . $username . "'";
		$result = mysqli_query($conn, $query);
		if (!$result) {
			return false;
		} else {
			$row = mysqli_fetch_assoc($result);
			$user = new User();
			$user->setUsername($row['username']);
			$user->setFirstName($row['first_name']);
			$user->setLastName($row['last_name']);
			$user->setEmail($row['email']);
			$user->setRole($row['role']);
			mysqli_free_result($result);
			$mySQLConn->closeDatabaseConnection($conn);
			return $user;
		}
	}

	public function findAll() {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$result = mysqli_query($conn, 'SELECT username, first_name, last_name, email, role FROM user');
		if (!$result) {
			return false;
		} else {
			$users = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$user = new User();
				$user->setUsername($row['username']);
				$user->setFirstName($row['first_name']);
				$user->setLastName($row['last_name']);
				$user->setEmail($row['email']);
				$user->setRole($row['role']);
				$users[] = $user;
			}
			mysqli_free_result($result);
			$mySQLConn->closeDatabaseConnection($conn);
			return $users;
		}
	}

	public function findByUsernamePassword($username, $password) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$query = "SELECT username, first_name, last_name, email, role FROM user WHERE username = '" . $username . "' and password = '" . $password . "'";
		$result = mysqli_query($conn, $query);
		if (!$result) {
			return false;
		} else {
			$row = mysqli_fetch_assoc($result);
			$user = new User();
			$user->setUsername($row['username']);
			$user->setFirstName($row['first_name']);
			$user->setLastName($row['last_name']);
			$user->setEmail($row['email']);
			$user->setRole($row['role']);
			mysqli_free_result($result);
			$mySQLConn->closeDatabaseConnection($conn);
			return $user;
		}
	}

	public function merge($object) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$query = "UPDATE user SET first_name='" . $object->getFirstName() . "', last_name='" . $object->getLastName() . "', email='" . $object->getEmail() . "', role='" . $object->getRole() . "' WHERE username='" . $object->getUsername() . "'";
		$result = mysqli_query($conn, $query);
		if (!$result) {
			return false;
		} else {
			$mySQLConn->closeDatabaseConnection($conn);
			return $result;
		}
	}

	public function persist($object) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$query = "INSERT INTO user (username, first_name, last_name, email, password, role) VALUES ('" . $object->getUsername() . "','" . $object->getFirstName() . "', '" . $object->getLastName() . "', '" . $object->getEmail() . "', '" . $object->getPassword() . "', '" . $object->getRole() . "')";
		$result = mysqli_query($conn, $query);
		if (!$result) {
			return false;
		} else {
			$mySQLConn->closeDatabaseConnection($conn);
			return $result;
		}
	}

	public function remove($username) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$result = mysqli_query($conn, "DELETE FROM user WHERE username='" . $username . "'");
		if (!$result) {
			return false;
		} else {
			$mySQLConn->closeDatabaseConnection($conn);
			return $result;
		}
	}

	public function findByEmail($email) {
		$mySQLConn = new MySQLConnection();
		$conn = $mySQLConn->openDatabaseConnection();
		$query = "SELECT username, first_name, last_name, email, role FROM user WHERE email = '" . $email . "'";
		$result = mysqli_query($conn, $query);
		if (!$result) {
			return false;
		} else {
			$row = mysqli_fetch_assoc($result);
			$user = new User();
			$user->setUsername($row['username']);
			$user->setFirstName($row['first_name']);
			$user->setLastName($row['last_name']);
			$user->setEmail($row['email']);
			$user->setRole($row['role']);
			mysqli_free_result($result);
			$mySQLConn->closeDatabaseConnection($conn);
			return $user;
		}
	}

}
