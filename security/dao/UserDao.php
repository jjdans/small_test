<?php

/**
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
interface UserDao {

	public function persist($object);

	public function merge($object);

	public function remove($username);

	public function find($username);

	public function findAll();
	
	public function findByUsernamePassword($username, $password);
	
	public function findByEmail($email);

}
