<?php

require_once 'common\domain\EntityItem.php';

/**
 * Description of User
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class User implements EntityItem {

	private $username;
	private $firstName;
	private $lastName;
	private $email;
	private $password;
	private $role;

	/**
	 * Get table
	 * 
	 * @return string
	 */
	public function getTable() {
		return "user";
	}

	/**
	 * Get username
	 * 
	 * @return string
	 */
	function getUsername() {
		return $this->username;
	}

	/**
	 * Get firstName
	 * 
	 * @return string
	 */
	function getFirstName() {
		return $this->firstName;
	}

	/**
	 * Get lastName
	 * 
	 * @return string
	 */
	function getLastName() {
		return $this->lastName;
	}

	/**
	 * Get email
	 * 
	 * @return string
	 */
	function getEmail() {
		return $this->email;
	}

	/**
	 * Get password
	 * 
	 * @return string
	 */
	function getPassword() {
		return $this->password;
	}

	/**
	 * Get role
	 * 
	 * @return string
	 */
	function getRole() {
		return $this->role;
	}

	/**
	 * Set username
	 * 
	 * @param string $username
	 */
	function setUsername($username) {
		$this->username = $username;
	}

	/**
	 * Set firstName
	 * 
	 * @param string $firstName
	 */
	function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	/**
	 * Set lastName
	 * 
	 * @param string $lastName
	 */
	function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	/**
	 * Set email
	 * 
	 * @param string $email
	 */
	function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * Set password
	 * 
	 * @param string $password
	 */
	function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * Set role
	 * 
	 * @param string $role
	 */
	function setRole($role) {
		$this->role = $role;
	}

	/**
	 * Get id
	 * 
	 * @return string
	 */
	public function getId() {
		return $this->username;
	}
	
	
	public function equals($obj) {
        if ($obj == null) {
            return false;
        }
        return $this->username == $obj->getUsername();
    }

	/**
	 * Check if the user tole is ADMIN
	 * 
	 * @return boolean
	 */
	public function isAdmin() {
		return $this->role == null ? false : $this->role == 'ADMIN';
	}

	/**
	 * Check if the user role is SUPERVISOR
	 * 
	 * @return boolean
	 */
	public function isSupervisor() {
		return $this->role == null ? false : $this->role == 'SUPERVISOR';
	}

}
