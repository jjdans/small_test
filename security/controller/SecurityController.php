<?php

require_once 'common\controller\AbstractController.php';
require_once 'security\manager\UserManagerImpl.php';
require_once 'common\service\Session.php';
require_once 'common\vo\Result.php';

/**
 * Description of SecurityController
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class SecurityController extends AbstractController {

	private $userManager;

	public function SecurityController() {
		$this->userManager = new UserManagerImpl();
	}

	public static function indexAction() {
		if (is_null(SecurityHelper::getSessionUser())) {
			$host = $_SERVER['HTTP_HOST'];
			$uri = '/SmallTest/index.php/login';
			header("Location: http://{$host}{$uri}");
		} else {
			require 'web\common\main.php';
		}
	}

	public function loginAction() {
		$data = '';
		if ($_SERVER['REQUEST_METHOD'] == 'GET') {
			require 'web\security\login.php';
		} else {
			$result = $this->userManager->findByUsernamePassword($_POST['username'], $_POST['password']);
			if ($result->isSuccess()) {
				$user = $result->getData();
				$session = new Session();
				$session->setAttribute(SecurityHelper::$SESSION_ATTRIB_USERNAME, $user->getUsername());
				$session->setAttribute(SecurityHelper::$SESSION_ATTRIB_FIRST_NAME, $user->getFirstName());
				$session->setAttribute(SecurityHelper::$SESSION_ATTRIB_LAST_NAME, $user->getLastName());
				$session->setAttribute(SecurityHelper::$SESSION_ATTRIB_ROLE, $user->getRole());
				$host = $_SERVER['HTTP_HOST'];
				$uri = '/SmallTest/index.php';
				header("Location: http://{$host}{$uri}");
			} else {
				$code = $result->getData();
				if ($code === '501') {
					$data = $result->getMsg();
					require 'web\security\login.php';
				} else {
					$page = $this->getErrorPage($code);
					require $page;
				}
			}
		}
	}

	public function logoutAction() {
		$session = new Session();
		$session->destroy();
		$host = $_SERVER['HTTP_HOST'];
		$uri = '/SmallTest/index.php';
		header("Location: http://{$host}{$uri}");
	}

}