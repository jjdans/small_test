<?php

require_once 'common\controller\AbstractController.php';
require_once 'security\manager\UserManagerImpl.php';
require_once 'security\service\SecurityHelper.php';
require_once 'common\vo\Result.php';
require_once 'common\service\SessionHelper.php';

/**
 * Description of UserController
 *
 * @author Juan Javier Dans Moreno
 * @email jjdans@nauta.cu and jjdans87@gmail.com
 */
class UserController extends AbstractController {

	private $userManager;

	public function UserController() {
		$this->userManager = new UserManagerImpl();
	}

	public function findAction() {
		$result = $this->userManager->find($_GET['id'], SecurityHelper::getSessionUser());
		if ($result->isSuccess()) {
			$user = $result->getData();
			require 'web\security\userShow.php';
		} else {
			$code = $result->getData();
			$page = $this->getErrorPage($code);
			require $page;
		}
	}

	public function findAllAction() {
		$result = $this->userManager->findAll(SecurityHelper::getSessionUser());
		if ($result->isSuccess()) {
			$users = $result->getData();
			require 'web\security\userList.php';
		} else {
			$code = $result->getData();
			$page = $this->getErrorPage($code);
			require $page;
		}
	}

	public function newAction() {
		if (SecurityHelper::getSessionUser() != null || SecurityHelper::getSessionRole() != 'ADMIN') {
			$msg = '';
			if ($_SERVER['REQUEST_METHOD'] == 'GET') {
				require 'web\security\userNew.php';
			} else {
				if ($_POST['password'] === $_POST['retype_password']) {
					$result = $this->userManager->store($_POST['username'], $_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['password'], $_POST['role'], SecurityHelper::getSessionUser());
					if ($result->isSuccess()) {
						$data = $result->getMsg();
						SessionHelper::getFlash($data);
						$host = $_SERVER['HTTP_HOST'];
						$uri = '/SmallTest/index.php/user/new';
						header("Location: http://{$host}{$uri}");
					} else {
						$code = $result->getData();
						if ($code === '501') {
							$msg = $result->getMsg();
							require 'web\security\userNew.php';
						} else {
							$page = $this->getErrorPage($code);
							require $page;
						}
					}
				} else {
					$msg = "The password and retype password are different.";
					require 'web\security\userNew.php';
				}
			}
		} else {
			require 'web\common\error403.php';
		}
	}

	public function editAction() {
		if (SecurityHelper::getSessionUser() != null || SecurityHelper::getSessionRole() != 'ADMIN') {
			$msg = '';
			if ($_SERVER['REQUEST_METHOD'] == 'GET') {
				$result = $this->userManager->find($_GET['id'], SecurityHelper::getSessionUser());
				if ($result->isSuccess()) {
					$user = $result->getData();
					require 'web\security\userEdit.php';
				} else {
					$code = $result->getData();
					$page = $this->getErrorPage($code);
					require $page;
				}
			} else {
				$result = $this->userManager->store($_POST['username'], $_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['password'], $_POST['role'], SecurityHelper::getSessionUser());
				if ($result->isSuccess()) {
					$data = $result->getMsg();
					SessionHelper::getFlash($data);
					$host = $_SERVER['HTTP_HOST'];
					$uri = '/SmallTest/index.php/user/edit?id=' . $_POST['username'];
					header("Location: http://{$host}{$uri}");
				} else {
					$code = $result->getData();
					if ($code === '501') {
						$data = $result->getMsg();
						SessionHelper::getFlash($data);
						$host = $_SERVER['HTTP_HOST'];
						$uri = '/SmallTest/index.php/user';
						header("Location: http://{$host}{$uri}");
					} else {
						$page = $this->getErrorPage($code);
						require $page;
					}
				}
			}
		} else {
			require 'web\common\error403.php';
		}
	}

	public function removeAction() {
		if (SecurityHelper::getSessionUser() != null || SecurityHelper::getSessionRole() != 'ADMIN') {
			if ($_SERVER['REQUEST_METHOD'] == 'GET') {
				$result = $this->userManager->find($_GET['id'], SecurityHelper::getSessionUser());
				if ($result->isSuccess()) {
					$user = $result->getData();
					require 'web\security\userDelete.php';
				} else {
					$code = $result->getData();
					$page = $this->getErrorPage($code);
					require $page;
				}
			} else {
				$result = $this->userManager->remove($_POST['id'], SecurityHelper::getSessionUser());
				if ($result->isSuccess()) {
					$data = $result->getMsg();
					SessionHelper::getFlash($data);
					$host = $_SERVER['HTTP_HOST'];
					$uri = '/SmallTest/index.php/user';
					header("Location: http://{$host}{$uri}");
				} else {
					$code = $result->getData();
					if ($code === '501') {
						$data = $result->getMsg();
						SessionHelper::getFlash($data);
						$host = $_SERVER['HTTP_HOST'];
						$uri = '/SmallTest/index.php/user';
						header("Location: http://{$host}{$uri}");
					} else {
						$page = $this->getErrorPage($code);
						require $page;
					}
				}
			}
		} else {
			require 'web\common\error403.php';
		}
	}

}
