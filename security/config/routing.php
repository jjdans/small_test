<?php

// route the request internally 
$uri = $_SERVER['REQUEST_URI'];

// proyect name
$proyectName = '/SmallTest';

if ($proyectName . '/index.php/user' == $uri) {
	$userController = new UserController();
	$userController->findAllAction();
} elseif ($proyectName . '/index.php/user/new' == $uri) {
	$userController = new UserController();
	$userController->newAction();
} elseif ($proyectName . '/index.php/user/edit' == $uri) {
	$userController = new UserController();
	$userController->editAction();
} elseif ($proyectName . '/index.php/user/delete' == $uri) {
	$userController = new UserController();
	$userController->removeAction();
} elseif (isset($_GET['id'])) {
	if ($proyectName . '/index.php/user/show?id=' . $_GET['id'] == $uri) {
		$userController = new UserController();
		$userController->findAction();
	} elseif ($proyectName . '/index.php/user/edit?id=' . $_GET['id'] == $uri) {
		$userController = new UserController();
		$userController->editAction();
	} elseif ($proyectName . '/index.php/user/delete?id=' . $_GET['id'] == $uri) {
		$userController = new UserController();
		$userController->removeAction();
	} else {
		require 'web/common/error404.php';
	}
} else {
	require 'web/common/error404.php';
}