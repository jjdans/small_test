<?php $title = $post->getTitle() ?>

<?php ob_start() ?> 
<h1>Datos del post</h1>
<table>
	<tbody>
		<tr>
			<th id="title">T&iacute;tulo:</th>
			<td headers="title"><?php echo $post->getTitle() ?></td>
		</tr>
		<tr>
			<th id="date">Fecha:</th>
			<td headers="date"><?php echo $post->getDate() ?></td>
		</tr>
		<tr>
			<th id="body">Cuerpo:</th>
			<td headers="body"><?php echo $post->getBody() ?></td>
		</tr>
	</tbody>
</table>
<div>
	<a href="edit?id=<?php echo $post->getId() ?>">Modificar</a>
	<a href="delete?id=<?php echo $post->getId() ?>">Eliminar</a>
	<a href="../post">Ir a la lista</a>
</div>
<?php $content = ob_get_clean() ?>

<?php include 'web\templates\layout.php' ?>