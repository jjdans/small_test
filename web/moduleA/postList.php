<?php $title = 'Posts' ?>

<?php ob_start() ?> 

<h1>Listado de Posts</h1> 
<div><?php echo SessionHelper::getFlash() ?></div></br>
<a href="post/new">Nuevo post</a>
<table>
	<thead>
		<tr>
			<th id="title">T&iacute;tulo</th>
			<th id="date">Fecha</th>
			<th id="body">Cuerpo</th>
			<th id="actions">Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($posts as $post): ?> 
			<tr>
				<td headers="title"><?php echo $post->getTitle() ?></td>
				<td headers="date"><?php echo $post->getDate() ?></td>
				<td headers="body"><?php echo $post->getBody() ?></td>
				<td headers="actions"><a href="post/show?id=<?php echo $post->getId() ?>">Ver</a> 
					| <a href="post/edit?id=<?php echo $post->getId() ?>">Modificar</a> 
					<?php if(SecurityHelper::getSessionRole() === 'ADMIN' || SecurityHelper::getSessionRole() === 'SUPERVISOR'){
						echo '| <a href="post/delete?id='.$post->getId().'">Eliminar</a>';
					}?>
				</td>
			</tr>
		<?php endforeach; ?> 
	</tbody>
</table> 
</br>
<div>
	<a href="/SmallTest/index.php">Panel de Control</a>
</div>
</br>
<?php $content = ob_get_clean() ?>

<?php include 'web\templates\layout.php' ?>

