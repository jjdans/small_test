<?php $title = 'Nuevo' ?>

<?php ob_start() ?> 
<h1>Nuevo post</h1>
<div class="msg"><?php echo $msg ?></div>
<div><?php echo SessionHelper::getFlash() ?></div>
<form action="/SmallTest/index.php/post/new" method="post">
	<div>
		<label for="title">T&iacute;tulo:</label>
		<input type="text" id="title" name="title" value="" required="required"/>
	</div>
	<div>
		<label for="date">Date:</label>
		<input type="date" id="date" name="date" value=""/>
	</div>
	<label for="body">Body:</label>
	<textarea id="body" name="body"></textarea>
	<div>
		<input class="button_enviar" type="submit"/>
		<a href="../post">Ir a la lista</a>
	</div>
</form>
<?php $content = ob_get_clean() ?>

<?php include 'web\templates\layout.php' ?>

