<?php $title = 'Editar' ?>

<?php ob_start() ?> 
<h1>Modificar post</h1>
<div class="msg"><?php echo $msg ?></div>
<div><?php echo SessionHelper::getFlash() ?></div>
<form action="/SmallTest/index.php/post/edit" method="post">
	<input type="hidden" name="id" value="<?php echo $post->getId() ?>" required="required" readonly="readonly"/>
	<div>
		<label for="title">T&iacute;tulo:</label>
		<input type="text" id="title" name="title" value="<?php echo $post->getTitle() ?>" required="required"/>
	</div>
	<div>
		<label for="date">Date:</label>
		<input type="date" id="date" name="date" value="<?php echo $post->getDate() ?>"/>
	</div>
	<label for="body">Body:</label>
	<textarea id="body" name="body"><?php echo $post->getBody() ?></textarea>
	<div>
		<input class="button_enviar" type="submit" value="Guardar"/>
		<a href="delete?id=<?php echo $post->getId() ?>">Eliminar</a>
		<a href="../post">Ir a la lista</a>
	</div>
</form>
<?php $content = ob_get_clean() ?>

<?php include 'web\templates\layout.php' ?>

