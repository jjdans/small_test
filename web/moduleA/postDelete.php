<?php $title = 'Eliminar' ?>

<?php ob_start() ?> 
	<h1>Eliminar post</h1>
	<h3>Est&aacute; seguro que desea eliminar este post?</h3>
	<div>
		<?php echo $post->getTitle() ?>
	</div>
	<div>
		<form action="/SmallTest/index.php/post/delete" method="post">
			<input type="hidden" name="id" value="<?php echo $post->getId() ?>" required="required"/>
			<input type="submit" value="Eliminar"/>
			<a href="/SmallTest/index.php/post">Volver al listado</a>
		</form>
	</div>
<?php $content = ob_get_clean() ?>

<?php include 'web\templates\layout.php' ?>