<!DOCTYPE html> 
<html> 
	<head> 
		<title>Small Test - <?php echo $title ?></title> 
		<style>
			#user-box{
				width: 80%;
				float: left;
			}
		</style>
	</head> 
	<body> 
		<header>Mi sitio web</header>
		<div id="user-box">
			<div>Bienvenido (a): <?php echo SecurityHelper::getSessionFullName() ?></div>
		</div>
		<a href="/SmallTest/index.php/logout" title="Cerrar sesión">Cerrar sesi&oacute;n</a>
		<?php echo $content ?> 
	</body> 
	<footer>Mi footer</footer>
</html>