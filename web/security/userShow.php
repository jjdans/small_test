<?php $title = $user->getUsername() ?>

<?php ob_start() ?> 
<h1>Datos del usuario</h1>
<table>
	<tbody>
		<tr>
			<th id="first_name">First Name:</th>
			<td headers="first_name"><?php echo $user->getFirstName() ?></td>
		</tr>
		<tr>
			<th id="last_name">Last Name:</th>
			<td headers="last_name"><?php echo $user->getLastName() ?></td>
		</tr>
		<tr>
			<th id="username">Username:</th>
			<td headers="username"><?php echo $user->getUsername() ?></td>
		</tr>
		<tr>
			<th id="email">Email:</th>
			<td headers="email"><?php echo $user->getEmail() ?></td>
		</tr>
		<tr>
			<th id="role">Role:</th>
			<td headers="role"><?php echo $user->getRole() ?></td>
		</tr>
	</tbody>
</table>
<div>
	<a href="edit?id=<?php echo $user->getId() ?>">Modificar</a>
	<a href="delete?id=<?php echo $user->getId() ?>">Eliminar</a>
	<a href="../user">Ir a la lista</a>
</div>
<?php $content = ob_get_clean() ?>

<?php include 'web/templates/layout.php' ?>