<?php $title = 'Eliminar' ?>

<?php ob_start() ?> 
<h1>Eliminar usuario</h1>
<h3>Est&aacute; seguro que desea eliminar este usuario?</h3>
<div>
	<?php echo $user->getFirstName() . ' ' . $user->getLastName() . ' - ' . $user->getUsername() ?>
</div>
<br>
<div>
	<form action="/SmallTest/index.php/user/delete" method="post">
		<input type="hidden" name="id" value="<?php echo $user->getId() ?>" required="required"/>
		<input type="submit" value="Eliminar"/>
		<a href="/SmallTest/index.php/user">Volver al listado</a>
	</form>
</div>
<br>
<?php $content = ob_get_clean() ?>

<?php include 'web/templates/layout.php' ?>