<?php $title = 'Nuevo' ?>

<?php ob_start() ?> 
<h1>Nuevo user</h1>
<div class="msg"><?php echo $msg ?></div>
<div><?php echo SessionHelper::getFlash() ?></div>
<form action="/SmallTest/index.php/user/new" method="POST">
	<div>
		<label for="first_name">First Name</label>
		<input type="text" id="first_name" name="first_name" value="" required="required"/>
	</div>
	<div>
		<label for="last_name">Last Name</label>
		<input type="text" id="last_name" name="last_name" value=""/>
	</div>
	<br>
	<div>
		<label for="username">Username:</label>
		<input type="text" id="username" name="username" value="" required="required"/>
	</div>
	<div>
		<label for="email">Email:</label>
		<input type="email" id="email" name="email" value="" required="required"/>
	</div>
	<br>
	<div>
		<label for="role">Role</label>
		<select id="role" name="role">
			<option value="USER" selected="selected">USER</option>
			<option value="SUPERVISOR">SUPERVISOR</option>
			<option value="ADMIN">ADMIN</option>
		</select>
	</div>
	<br>
	<div>
		<label for="password">Password</label>
		<input type="password" id="password" name="password" required="required"/>
	</div>
	<div>
		<label for="retype_password">Retype password</label>
		<input type="password" id="retype_password" name="retype_password" required="required"/>
	</div>
	<br>
	<div>
		<input class="button_enviar" type="submit"/>
		<a href="../user">Ir a la lista</a>
	</div>
</form>
<?php $content = ob_get_clean() ?>

<?php include 'web/templates/layout.php' ?>