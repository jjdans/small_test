<?php $title = 'Users'?>

<?php ob_start() ?> 

<h1>Listado de Usuarios</h1> 
<div><?php echo SessionHelper::getFlash() ?></div></br>
<a href="user/new">Nuevo user</a>
<table>
	<thead>
	<tr>
		<th id="username">Username</th>
        <th id="first_name">First Name</th>
        <th id="last_name">Last Name</th>
		<th id="email">Email</th>
		<th id="role">Role</th>
        <th id="actions">Acciones</th>
	</tr>
   </thead>
   <tbody>
	   <?php foreach ($users as $user): ?> 
	   <tr>
		   <td headers="first_name"><?php echo $user->getFirstName() ?></td>
		   <td headers="last_name"><?php echo $user->getLastName() ?></td>
		   <td headers="username"><?php echo $user->getUsername() ?></td>
		   <td headers="email"><?php echo $user->getEmail() ?></td>
		   <td headers="role"><?php echo $user->getRole() ?></td>
		   <td headers="actions"><a href="user/show?id=<?php echo $user->getId() ?>">Ver</a> 
			   | <a href="user/edit?id=<?php echo $user->getId() ?>">Modificar</a> 
			   | <a href="user/delete?id=<?php echo $user->getId() ?>">Eliminar</a></td>
	   </tr>
	   <?php endforeach; ?> 
   </tbody>
</table> 
</br>
<div>
	<a href="/SmallTest/index.php">Panel de Control</a>
</div>
</br>
<?php $content = ob_get_clean() ?>

<?php include 'web/templates/layout.php' ?>
