<?php $title = 'Editar' ?>

<?php ob_start() ?> 
<h1>Modificar usuario</h1>
<div class="msg"><?php echo $msg ?></div>
<div><?php echo SessionHelper::getFlash() ?></div>
<form action="/SmallTest/index.php/user/edit" method="POST">
	<div>
		<label for="first_name">First Name:</label>
		<input type="text" id="first_name" name="first_name" value="<?php echo $user->getFirstName() ?>" required="required"/>
	</div>
	<div>
		<label for="last_name">Last Name</label>
		<input type="text" id="last_name" name="last_name" value="<?php echo $user->getLastName() ?>"/>
	</div>
	<br>
	<div>
		<label for="username">Username:</label>
		<input type="text" id="username" name="username" value="<?php echo $user->getUsername() ?>" required="required" readonly="readonly"/>
	</div>
	<div>
		<label for="email">Email:</label>
		<input type="email" id="email" name="email" value="<?php echo $user->getEmail() ?>" required="required"/>
	</div>
	<br>
	<div>
		<label for="role">Role</label>
		<select id="role" name="role">
			<option value="USER" <?php echo ($user->getRole() === 'USER' ? 'selected' : '') ?>>USER</option>
			<option value="SUPERVISOR" <?php echo ($user->getRole() === 'SUPERVISOR' ? 'selected' : '') ?>>SUPERVISOR</option>
			<option value="ADMIN" <?php echo ($user->getRole() === 'ADMIN' ? 'selected' : '') ?>>ADMIN</option>
		</select>
	</div>
	<br>
	<div>
		<input class="button_enviar" type="submit" value="Guardar"/>
		<a href="delete?id=<?php echo $user->getId() ?>">Eliminar</a>
		<a href="../user">Ir a la lista</a>
	</div>
</form>
<br>
<?php $content = ob_get_clean() ?>

<?php include 'web/templates/layout.php' ?>