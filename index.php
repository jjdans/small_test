<?php

require_once 'security\controller\SecurityController.php';
require_once 'moduleA\controller\PostController.php';
require_once 'security\controller\UserController.php';

// route the request internally 
$uri = $_SERVER['REQUEST_URI'];

// proyect name
$proyectName = '/SmallTest';

if ($proyectName . '/index.php' == $uri || $proyectName . '/' == $uri) {
	SecurityController::indexAction();
} elseif (strpos($uri, "post") !== false) {
	if (SecurityHelper::getSessionUser() != null) {
		require 'moduleA/config/routing.php';
	} else {
		require 'web/common/error403.php';
	}
} elseif ((strpos($uri, "login") !== false) || (strpos($uri, "logout") !== false)) {
	require 'common/config/routing.php';
} elseif (strpos($uri, "user") !== false) {
	if (SecurityHelper::getSessionUser() != null && SecurityHelper::getSessionRole() === 'ADMIN') {
		require 'security/config/routing.php';
	} else {
		require 'web/common/error403.php';
	}
} else {
	require 'web/common/error404.php';
}